﻿using dal_crud;
using Microsoft.AspNetCore.Mvc;

namespace mvc.Controllers
{
    public class BookController : Controller
    {
        private BookRepository Repo;

        public BookController(BookRepository repo)
        {
            this.Repo = repo;
        }
        public IActionResult index()
        {
            var b = Repo.GetBooks();
            return View(b);
        }

        public IActionResult create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult create(Book book)
        {
            var b = Repo.Add(book);
            return RedirectToAction("index");
        }
        public IActionResult edit(int id)
        {
            Book b = Repo.GetBook(id);
            return View(b);
        }

        [HttpPost]
        public IActionResult edit(int id, Book book)
        {
            int b = Repo.Update(book);
            return RedirectToAction("index");
        }

        public IActionResult delete(int id)
        {
            Book b = Repo.GetBook(id);
            return View(b);
        }
        [HttpPost]
        public IActionResult confirmdelete(int id)
        {
            int b = Repo.Delete(id);
            return RedirectToAction("index");
        }

        public IActionResult details(int id)
        {
            Book b = Repo.GetBook(id);
            return View(b);
        }


    }
}
