CREATE TABLE `book` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`Title` VARCHAR(250) NOT NULL,
	`Author` VARCHAR(250) NOT NULL,
	`PublishedDate` DATETIME NOT NULL,
	PRIMARY KEY (`ID`)
);

insert into book(title,author,publisheddate)
values('The Power of Habit','Charles Duhigg','2013-04-19');
