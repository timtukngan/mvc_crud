create table book( ID int identity(1,1) primary key, Title varchar(250) not null, Author varchar(250) not null, PublishedDate datetime not null);

insert into book(title,author,publisheddate)
values('The Power of Habit','Charles Duhigg','2013-04-19')