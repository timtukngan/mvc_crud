﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;

namespace dal_crud
{
    public class DbConn
    {
        public DbConn(DbConnection connection)
        {
            this.Connection = connection;
        }

        internal DbConnection Connection { get; }

    }

    public class BookRepository
    {
        private DbConn DB;

        public BookRepository(DbConn dbConn)
        {
            this.DB = dbConn;
        }

        public IEnumerable<Book> GetBooks()
        {
            var books = DB.Connection.Query<Book>("select * from book");
            return books;
        }

        public Book GetBook(int id)
        {
            var books = DB.Connection.QuerySingle<Book>("select * from book where id=@id", new { id });
            return books;
        }

        public int Add(Book newbook)
        {
            var ind = DB.Connection.Execute("insert into book(title,author,publisheddate) values(@title,@author,@publisheddate)", newbook);
            return ind;
        }

        public int Delete(int id)
        {
            var ind = DB.Connection.Execute("delete from book where id=@id", new { id });
            return ind;
        }

        public int Update(Book book)
        {
            var ind = DB.Connection.Execute("update book  set title=@title, author=@author, publisheddate=@publisheddate where id=@id", book);
            return ind;
        }
    }
    public class Book
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublishedDate { get; set; }
    }

}
